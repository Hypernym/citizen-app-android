package com.hypernym.citizenapp.toolbox;


public interface ToolbarListener {
    void setTitle(String title);
}
