package com.hypernym.citizenapp.fragments;


import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.daimajia.slider.library.Tricks.ViewPagerEx;
import com.hypernym.citizenapp.FrameActivity;
import com.hypernym.citizenapp.LoginActivity;
import com.hypernym.citizenapp.R;
import com.hypernym.citizenapp.adapter.DashBoardAdapter;
import com.hypernym.citizenapp.model.ItemData;
import com.hypernym.citizenapp.toolbox.OnItemClickListener;
import com.hypernym.citizenapp.toolbox.ToolbarListener;
import com.hypernym.citizenapp.utils.ActivityUtils;
import com.hypernym.citizenapp.utils.ItemDecorationAlbumColumns;
import com.hypernym.citizenapp.utils.LoginUtils;

import java.util.HashMap;

/**
 * Created by Bilal Rashid on 10/10/2017.
 */

public class HomeFragment extends Fragment implements View.OnClickListener, OnItemClickListener, BaseSliderView.OnSliderClickListener, ViewPagerEx.OnPageChangeListener {
    private ViewHolder mHolder;
    DashBoardAdapter dashBoardAdapter;
    AlertDialog alertDialog;
    Intent callIntent;
    HashMap<String, Integer> Hash_file_maps;
    Dialog dialogEmergency;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
//        if (!EventBus.getDefault().isRegistered(this))
//            EventBus.getDefault().register(this);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        View view, view1;

        inflater.inflate(R.menu.menu_dashboard, menu);
        view = menu.findItem(R.id.help).getActionView();
        view1 = menu.findItem(R.id.phone).getActionView();

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getContext(), "help", Toast.LENGTH_SHORT).show();

            }
        });
        view1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getContext(), "phone", Toast.LENGTH_SHORT).show();

            }
        });
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof ToolbarListener) {
            ((ToolbarListener) context).setTitle("");
        }
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_home, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mHolder = new ViewHolder(view);
        ItemData itemsData[] = {

                new ItemData("Report Incident", R.drawable.ic_report, ""),
                new ItemData("Emergency", R.drawable.ic_emergency, ""),
                new ItemData("Map", R.drawable.ic_policestation_map, ""),
                new ItemData("Important Numbers", R.drawable.ic_importantnumber, ""),
                //  new ItemData("Services", R.drawable.ic_launcher, ""),
                new ItemData("Police Stations", R.drawable.ic_policestation, ""),
                // new ItemData("Volunteers", R.drawable.ic_launcher, ""),
                new ItemData("Downloads", R.drawable.ic_document, ""),
                new ItemData("FM Radio", R.drawable.ic_radio, ""),
                new ItemData("Profile", R.drawable.ic_profile, ""),
                new ItemData("E ticket", R.drawable.ic_help, "")
        };
        Hash_file_maps = new HashMap<String, Integer>();
        RecyclerSetup(itemsData);
        setupSlider();

    }

    private void setupSlider() {
        Hash_file_maps.put("Police", R.drawable.banner1);
        Hash_file_maps.put("Security", R.drawable.banner2);
//        Hash_file_maps.put("Android Donut", "https://images.pexels.com/photos/218983/pexels-photo-218983.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260");
//        Hash_file_maps.put("Android Eclair", "https://images.pexels.com/photos/218983/pexels-photo-218983.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260");
//        Hash_file_maps.put("Android Froyo", "https://images.pexels.com/photos/218983/pexels-photo-218983.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260");
//        Hash_file_maps.put("Android GingerBread", "https://images.pexels.com/photos/218983/pexels-photo-218983.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260");

        for (String name : Hash_file_maps.keySet()) {

            TextSliderView textSliderView = new TextSliderView(getActivity());
            textSliderView
                    .description(name)
                    .image(Hash_file_maps.get(name))
                    .setScaleType(BaseSliderView.ScaleType.Fit)
                    .setOnSliderClickListener(this);
            textSliderView.bundle(new Bundle());
            textSliderView.getBundle()
                    .putString("extra", name);
            mHolder.sliderLayout.addSlider(textSliderView);
        }
        mHolder.sliderLayout.setPresetTransformer(SliderLayout.Transformer.Accordion);
        mHolder.sliderLayout.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
        mHolder.sliderLayout.setCustomAnimation(new DescriptionAnimation());
        mHolder.sliderLayout.setDuration(3000);
        mHolder.sliderLayout.addOnPageChangeListener(this);
    }

    private void RecyclerSetup(ItemData[] itemData) {
        mHolder.recycler_dashboard.setHasFixedSize(true);
        mHolder.recycler_dashboard.setLayoutManager(new GridLayoutManager(getContext(), 3));
        mHolder.recycler_dashboard.addItemDecoration(new ItemDecorationAlbumColumns(
                getResources().getDimensionPixelSize(R.dimen.spacing_medium), 3));

        // 3. create an adapter
        dashBoardAdapter = new DashBoardAdapter(itemData, this, getContext());
        // 4. set adapter
        mHolder.recycler_dashboard.setAdapter(dashBoardAdapter);
        // 5. set item animator to DefaultAnimator
        mHolder.recycler_dashboard.setItemAnimator(new DefaultItemAnimator());
    }

    @Override
    public void onClick(View view) {

    }

    @Override
    public void onItemClick(View view, Object data, int position) {
        if (position == 9) {
            ActivityUtils.startActivity(getContext(), FrameActivity.class, InstagramFragment.class.getName(), null, false);
//        } else if (position == 8) {
//            ActivityUtils.startActivity(getContext(), FrameActivity.class, TwitterFragment.class.getName(), null, false);
        } else if (position == 7) {
            ActivityUtils.startActivity(getContext(), FrameActivity.class, ProfileFragment.class.getName(), null, false);
        } else if (position == 3) {
            ActivityUtils.startActivity(getContext(), FrameActivity.class, ImportantNumberFragment.class.getName(), null, false);
        } else if (position == 0) {
            ActivityUtils.startActivity(getContext(), FrameActivity.class, ReportIncident2Fragment.class.getName(), null, false);
        } else if (position == 4) {
            ActivityUtils.startActivity(getContext(), FrameActivity.class, PoliceStationFragment.class.getName(), null, false);
        } else if (position == 5) {
            ActivityUtils.startActivity(getContext(), FrameActivity.class, DownloadPdfFragment.class.getName(), null, false);
        } else if (position == 1) {
         //  showDialog();
            showEmergencyDialog();
        } else if (position == 2) {
            ActivityUtils.startActivity(getContext(), FrameActivity.class, MapFragment.class.getName(), null, false);
        } else if (position == 6) {
            ActivityUtils.startActivity(getContext(), FrameActivity.class, RadioFragment.class.getName(), null, false);
        }


    }

    private void showDialog() {
        final CharSequence[] items = {"Emergency call", "Emergency Alert", "Women & Child Protection"};
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("Select Emergency");
        builder.setSingleChoiceItems(items, -1, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {


                switch (item) {
                    case 0:
                        callIntent = new Intent(Intent.ACTION_CALL);
                        callIntent.setData(Uri.parse("tel:15"));
                        startActivity(callIntent);
                        break;
                    case 2:
                        callIntent = new Intent(Intent.ACTION_CALL);
                        callIntent.setData(Uri.parse("tel:8090"));
                        startActivity(callIntent);
                        break;

                }
                alertDialog.dismiss();
            }
        });
        alertDialog = builder.create();
        alertDialog.show();
    }
    private void showEmergencyDialog() {
        dialogEmergency = new Dialog(getContext());
        dialogEmergency.setContentView(R.layout.dialog_emergency);
        LinearLayout linearLayout_ambulance=(LinearLayout)dialogEmergency.findViewById(R.id.layout_ambulance);
        LinearLayout linearLayout_alert=(LinearLayout)dialogEmergency.findViewById(R.id.layout_alert);
        LinearLayout linearLayout_women=(LinearLayout)dialogEmergency.findViewById(R.id.layout_womenProtection);
        linearLayout_ambulance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:1122"));
                startActivity(callIntent);
            }
        });
        linearLayout_alert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:15"));
                startActivity(callIntent);
            }
        });
        linearLayout_women.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:8090"));
                startActivity(callIntent);

            }
        });



      //  dialogEmergency.setCanceledOnTouchOutside(false);
        dialogEmergency.show();
        dialogEmergency.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
      //  dialogEmergency.setCancelable(false);
    }


    @Override
    public void onSliderClick(BaseSliderView slider) {

    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    public static class ViewHolder {


        RecyclerView recycler_dashboard;
        SliderLayout sliderLayout;

        public ViewHolder(View view) {
            recycler_dashboard = (RecyclerView) view.findViewById(R.id.recyclerView_dashboard);
            sliderLayout = (SliderLayout) view.findViewById(R.id.slider);


        }

    }



}
