package com.hypernym.citizenapp.fragments;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.github.rahatarmanahmed.cpv.CircularProgressView;
import com.github.ybq.android.spinkit.SpinKitView;
import com.github.ybq.android.spinkit.sprite.Sprite;
import com.github.ybq.android.spinkit.style.DoubleBounce;
import com.github.ybq.android.spinkit.style.PulseRing;
import com.github.ybq.android.spinkit.style.WanderingCubes;
import com.hypernym.citizenapp.FrameActivity;
import com.hypernym.citizenapp.R;
import com.hypernym.citizenapp.adapter.ImportantNumberAdapter;
import com.hypernym.citizenapp.adapter.PoliceStationAdapter;
import com.hypernym.citizenapp.api.ApiInterface;
import com.hypernym.citizenapp.model.ImportantNumberData;
import com.hypernym.citizenapp.model.ImportantNumberList;
import com.hypernym.citizenapp.model.ItemData;
import com.hypernym.citizenapp.model.PoliceStationData;
import com.hypernym.citizenapp.model.PoliceStationList;
import com.hypernym.citizenapp.toolbox.OnItemClickListener;
import com.hypernym.citizenapp.toolbox.ToolbarListener;
import com.hypernym.citizenapp.utils.AppUtils;
import com.hypernym.citizenapp.utils.Constants;
import com.hypernym.citizenapp.utils.ItemDecorationAlbumColumns;

import java.util.ArrayList;
import java.util.List;

import fr.ganfra.materialspinner.MaterialSpinner;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static android.Manifest.permission.CALL_PHONE;
import static android.Manifest.permission.CAMERA;
import static android.Manifest.permission.READ_EXTERNAL_STORAGE;

public class ImportantNumberFragment extends Fragment implements View.OnClickListener,
        OnItemClickListener, MaterialSpinner.OnItemSelectedListener {
    private ViewHolder mHolder;
    ImportantNumberAdapter NumberAdapter;
    ArrayList<ImportantNumberList> importantNumberLists;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    public static final int RequestPermissionCode = 1;
    String[] arraySpinner = new String[]{"All Numbers", "Other Numbers"};
    Object item;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof ToolbarListener) {
            ((ToolbarListener) context).setTitle("");
        }
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_important_numbers, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mHolder = new ViewHolder(view);
        AppUtils.hideKeyboard(getActivity());
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_item, arraySpinner);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mHolder.spinnerNumbers.setAdapter(adapter);
        mHolder.spinnerNumbers.setOnItemSelectedListener(this);
        gettingdata();
        mHolder.editTextImportantNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                filter(s.toString());
            }
        });

//        ItemData itemsData[] = {
//
//                new ItemData("Emergency Response", R.drawable.ic_launcher, "15"),
//                new ItemData("Emergency Response", R.drawable.ic_launcher, "15"),
//                new ItemData("Complaint", R.drawable.ic_launcher, "0519201081"),
//                new ItemData("Complaint", R.drawable.ic_launcher, "03331000015"),
//                new ItemData("Complaint", R.drawable.ic_launcher, "0519108191"),
//                new ItemData("Safe City Project", R.drawable.ic_launcher, "0519001520"),
//                new ItemData("Anti Corruption (Call)", R.drawable.ic_launcher, "03008507520"),
//                new ItemData("Anti Corruption (SMS only)", R.drawable.ic_launcher, "03338966689"),
//                new ItemData("Complaint", R.drawable.ic_launcher, "1416")
//        };
//        RecyclerSetup(itemsData);
    }

    private void filter(String text) {
        //new array list that will hold the filtered data
        ArrayList<ImportantNumberList> filterdNames = new ArrayList<>();

        //looping through existing elements
        for (ImportantNumberList s : importantNumberLists) {

            if (s.getName().toLowerCase().contains(text.toLowerCase())) {
                filterdNames.add(s);
            }
        }


        //calling a method of the adapter class and passing the filtered list
        NumberAdapter.filterList(filterdNames);
    }


    public void gettingdata() {
        Sprite doubleBounce = new WanderingCubes();
        mHolder.progressBar.setIndeterminateDrawable(doubleBounce);
        ApiInterface.retrofit.getimportantnumber().enqueue(new Callback<ImportantNumberData>() {
            @Override
            public void onResponse(Call<ImportantNumberData> call, Response<ImportantNumberData> response) {
                try {
                    if (response.isSuccessful()) {
                        mHolder.progressBar.setVisibility(View.GONE);
                        importantNumberLists = (ArrayList<ImportantNumberList>) response.body().getData();
                        RecyclerSetup();
                        mHolder.recycler_numbers.setAdapter(NumberAdapter);
                    } else {
                        mHolder.progressBar.setVisibility(View.GONE);
                        AppUtils.showSnackBar(getView(), "" + response.body().getErrorMsg());
                    }
                } catch (Exception ex) {
                    mHolder.progressBar.setVisibility(View.GONE);
                    AppUtils.showSnackBar(getView(), AppUtils.getErrorMessage(getContext(), 2));

                }
            }

            @Override
            public void onFailure(Call<ImportantNumberData> call, Throwable t) {
                AppUtils.showSnackBar(getView(), AppUtils.getErrorMessage(getContext(), Constants.NETWORK_ERROR));
                mHolder.progressBar.setVisibility(View.GONE);
            }
        });
    }

    private void RecyclerSetup() {
        mHolder.recycler_numbers.setHasFixedSize(true);
        mHolder.recycler_numbers.setLayoutManager(new GridLayoutManager(getContext(), 1));
        mHolder.recycler_numbers.addItemDecoration(new ItemDecorationAlbumColumns(
                getResources().getDimensionPixelSize(R.dimen.spacing_medium), 1));

        NumberAdapter = new ImportantNumberAdapter(importantNumberLists, this, getContext());
        mHolder.recycler_numbers.setAdapter(NumberAdapter);
        mHolder.recycler_numbers.setItemAnimator(new DefaultItemAnimator());
    }


    @Override
    public void onClick(View view) {

    }

    @Override
    public void onItemClick(View view, Object data, int position) {
        ImportantNumberList obj = (ImportantNumberList) data;
        switch (view.getId()) {
            case R.id.item_call:

                if (Checkpermission()) {
                    Intent callIntent = new Intent(Intent.ACTION_CALL);
                    callIntent.setData(Uri.parse("tel:" + obj.getNumber()));
                    startActivity(callIntent);
                } else {
                    AppUtils.showSnackBar(getView(), AppUtils.getErrorMessage(getContext(), 9));

                    requestlocationpermission();
                }

                break;
            case R.id.item_message:

                Intent callIntent_sms = new Intent(Intent.ACTION_VIEW);
                callIntent_sms.setData(Uri.parse("sms:" + obj.getNumber()));
                startActivity(callIntent_sms);

        }

    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        switch (parent.getId()) {
            case R.id.spinner:
                item = parent.getItemAtPosition(position);
                if (item.equals("All Numbers")) {
                } else {
                }
                break;
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    public static class ViewHolder {
        RecyclerView recycler_numbers;
        MaterialSpinner spinnerNumbers;
        EditText editTextImportantNumber;
        TextView textViewImportant,textViewNumber;
        SpinKitView progressBar;
        public ViewHolder(View view) {
            recycler_numbers = (RecyclerView) view.findViewById(R.id.recyclerView_important_numbers);
            progressBar = (SpinKitView)view.findViewById(R.id.spin_kit);
            spinnerNumbers = (MaterialSpinner) view.findViewById(R.id.spinner);
            editTextImportantNumber = (EditText) view.findViewById(R.id.edit_importantNumber);
            textViewImportant=(TextView)view.findViewById(R.id.lbl_important);
            textViewNumber=(TextView)view.findViewById(R.id.lbl_number);
            Typeface poppinsRegular = Typeface.createFromAsset(textViewImportant.getContext().getAssets(), "Poppins-Regular.ttf");
            Typeface poppinsmedium = Typeface.createFromAsset(textViewNumber.getContext().getAssets(), "Poppins-Medium.ttf");
            textViewImportant.setTypeface(poppinsRegular);
            textViewNumber.setTypeface(poppinsmedium);
        }
    }

    public boolean Checkpermission() {

        int CallPermissonResult = ContextCompat.checkSelfPermission(getContext(), CALL_PHONE);


        return
                CallPermissonResult == PackageManager.PERMISSION_GRANTED;
    }

    private void requestlocationpermission() {
        ActivityCompat.requestPermissions(getActivity(), new String[]
                {
                        ACCESS_FINE_LOCATION,
                        READ_EXTERNAL_STORAGE,
                        CALL_PHONE,
                        CAMERA


                }, RequestPermissionCode);


    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {

            case RequestPermissionCode:

                if (grantResults.length > 0) {

                    boolean CallPermission = grantResults[1] == PackageManager.PERMISSION_GRANTED;

                    if (CallPermission) {
                        // Toast.makeText(HomeActivity.this, "Permission Granted", Toast.LENGTH_LONG).show();
                    } else {
                        //Toast.makeText(HomeActivity.this,"Permission Denied",Toast.LENGTH_LONG).show();
                    }
                }

                break;
        }
    }


}