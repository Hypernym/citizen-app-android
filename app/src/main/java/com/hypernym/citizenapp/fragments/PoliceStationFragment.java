package com.hypernym.citizenapp.fragments;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.github.rahatarmanahmed.cpv.CircularProgressView;
import com.hypernym.citizenapp.R;
import com.hypernym.citizenapp.adapter.PoliceStationAdapter;
import com.hypernym.citizenapp.api.ApiInterface;
import com.hypernym.citizenapp.model.PoliceStationData;
import com.hypernym.citizenapp.model.PoliceStationList;
import com.hypernym.citizenapp.toolbox.OnItemClickListener;
import com.hypernym.citizenapp.toolbox.ToolbarListener;
import com.hypernym.citizenapp.utils.AppUtils;
import com.hypernym.citizenapp.utils.Constants;
import com.hypernym.citizenapp.utils.ItemDecorationAlbumColumns;


import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PoliceStationFragment extends Fragment implements View.OnClickListener, OnItemClickListener {

    private ViewHolder mHolder;
    List<PoliceStationList> policeStationLists;
    PoliceStationAdapter policeStationAdapter;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
//        if (!EventBus.getDefault().isRegistered(this))
//            EventBus.getDefault().register(this);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof ToolbarListener) {
            ((ToolbarListener) context).setTitle("");
        }
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_police_station, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mHolder = new ViewHolder(view);
        AppUtils.hideKeyboard(getActivity());
        mHolder.circularProgressView.startAnimation();
        mHolder.circularProgressView.setVisibility(View.VISIBLE);
        gettingdata();
    }


    @Override
    public void onClick(View view) {

    }

    public void gettingdata() {
        ApiInterface.retrofit.getpolicelist().enqueue(new Callback<PoliceStationData>() {
            @Override
            public void onResponse(Call<PoliceStationData> call, Response<PoliceStationData> response) {
                try {
                    if (response.isSuccessful()) {
                        mHolder.circularProgressView.stopAnimation();
                        mHolder.circularProgressView.setVisibility(View.GONE);
                        policeStationLists = response.body().getData();
                        RecyclerSetup();
                        mHolder.recycle_police_station.setAdapter(policeStationAdapter);
                    } else {
                        mHolder.circularProgressView.stopAnimation();
                        mHolder.circularProgressView.setVisibility(View.GONE);
                        AppUtils.showSnackBar(getView(), "" + response.body().getErrorMsg());
                    }
                } catch (Exception ex) {
                    mHolder.circularProgressView.stopAnimation();
                    mHolder.circularProgressView.setVisibility(View.GONE);
                    AppUtils.showSnackBar(getView(), AppUtils.getErrorMessage(getContext(), 2));

                }
            }

            @Override
            public void onFailure(Call<PoliceStationData> call, Throwable t) {
                AppUtils.showSnackBar(getView(), AppUtils.getErrorMessage(getContext(), Constants.NETWORK_ERROR));

                mHolder.circularProgressView.stopAnimation();
                mHolder.circularProgressView.setVisibility(View.GONE);
            }
        });


    }

    private void RecyclerSetup() {
        mHolder.recycle_police_station.setHasFixedSize(true);
        mHolder.recycle_police_station.setLayoutManager(new GridLayoutManager(getContext(), 1));
        mHolder.recycle_police_station.addItemDecoration(new ItemDecorationAlbumColumns(
                getResources().getDimensionPixelSize(R.dimen.spacing_medium), 1));
        // 3. create an adapter
        policeStationAdapter = new PoliceStationAdapter(policeStationLists, this, getContext());

        // 4. set adapter
        mHolder.recycle_police_station.setAdapter(policeStationAdapter);
        // 5. set item animator to DefaultAnimator
        mHolder.recycle_police_station.setItemAnimator(new DefaultItemAnimator());
    }

    @Override
    public void onItemClick(View view, Object data, int position) {
        switch (view.getId()) {
            case R.id.item_call:
                String phoneno = policeStationLists.get(position).getPhoneNo();
                Toast.makeText(getContext(), "" + phoneno, Toast.LENGTH_SHORT).show();
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:" + phoneno));
                startActivity(callIntent);
                break;
            case R.id.item_location:
                Uri uri = Uri.parse("https://maps.google.com/maps?q=loc:" + 33.6689488 + "," + 72.9939884 + "(" + policeStationLists.get(position).getName() + ")");
                Intent intentimp = new Intent(Intent.ACTION_VIEW, uri);
                intentimp.setFlags(getActivity().getIntent().FLAG_ACTIVITY_NEW_TASK);
                if (intentimp.resolveActivity(getActivity().getPackageManager()) != null) {
                    getActivity().startActivity(intentimp);
                }
        }
    }

    public static class ViewHolder {


        RecyclerView recycle_police_station;
        CircularProgressView circularProgressView;
        TextView textViewPolice,textViewStation;


        public ViewHolder(View view) {
            recycle_police_station = (RecyclerView) view.findViewById(R.id.recyclerView_police_station);
            circularProgressView = (CircularProgressView) view.findViewById(R.id.progress);
            textViewPolice=(TextView)view.findViewById(R.id.lbl_police);
            textViewStation=(TextView)view.findViewById(R.id.lbl_stations);
            Typeface poppinsRegular = Typeface.createFromAsset(textViewPolice.getContext().getAssets(), "Poppins-Regular.ttf");
            Typeface poppinsmedium = Typeface.createFromAsset(textViewStation.getContext().getAssets(), "Poppins-Medium.ttf");
            textViewPolice.setTypeface(poppinsRegular);
            textViewStation.setTypeface(poppinsmedium);
        }

    }
}