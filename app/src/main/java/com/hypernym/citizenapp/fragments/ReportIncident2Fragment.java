package com.hypernym.citizenapp.fragments;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.hypernym.citizenapp.R;
import com.hypernym.citizenapp.toolbox.ToolbarListener;
import com.hypernym.citizenapp.utils.AppUtils;

public class ReportIncident2Fragment extends Fragment implements View.OnClickListener {

    private ViewHolder mHolder;
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
//        if (!EventBus.getDefault().isRegistered(this))
//            EventBus.getDefault().register(this);
    }
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof ToolbarListener) {
            ((ToolbarListener) context).setTitle("");
        }
    }
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_report_incident2, container, false);
    }
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mHolder = new ReportIncident2Fragment.ViewHolder(view);
        AppUtils.hideKeyboard(getActivity());
    }


    @Override
    public void onClick(View view) {

    }

    public static class ViewHolder {

        TextView textViewImportant,textViewNumber,textViewInfo;
        public ViewHolder(View view) {
            textViewImportant=(TextView)view.findViewById(R.id.lbl_police);
            textViewNumber=(TextView)view.findViewById(R.id.lbl_stations);
            textViewInfo=(TextView)view.findViewById(R.id.lbl_info);
            Typeface poppinsRegular = Typeface.createFromAsset(textViewImportant.getContext().getAssets(), "Poppins-Regular.ttf");
            Typeface poppinsmedium = Typeface.createFromAsset(textViewNumber.getContext().getAssets(), "Poppins-Medium.ttf");
            textViewImportant.setTypeface(poppinsRegular);
            textViewInfo.setTypeface(poppinsmedium);
            textViewNumber.setTypeface(poppinsmedium);

        }

    }
}
