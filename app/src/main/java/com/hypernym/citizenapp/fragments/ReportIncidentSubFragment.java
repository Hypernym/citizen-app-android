package com.hypernym.citizenapp.fragments;

import android.content.ClipData;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.hypernym.citizenapp.R;
import com.hypernym.citizenapp.toolbox.ToolbarListener;
import com.hypernym.citizenapp.utils.AppUtils;
import com.hypernym.citizenapp.utils.Constants;
import com.hypernym.citizenapp.utils.FileUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static android.Manifest.permission.CALL_PHONE;
import static android.Manifest.permission.CAMERA;
import static android.Manifest.permission.READ_EXTERNAL_STORAGE;

public class ReportIncidentSubFragment extends Fragment implements View.OnClickListener {
    private boolean mIsProfileImageAdded = false;
    private static final int GALLERY = 2;
    private static final int CAMERAA = 1;
    private static final int VIDEO = 3;
    private static final int MIC = 4;
    private ViewHolder mHolder;
    String Titlename;
    File file_name;
    AlertDialog levelDialog;
    final CharSequence[] items = {" Choose From Gallery ", " Choose From Camera "};
    String mCurrentPhotoPath, globalImagePath;
    private Uri mSelectedProfileImageURI;
    private String mProfileImageDecodableString;
    String imgName;
    MultipartBody.Part partImage;
    public static final int RequestPermissionCode = 1;


    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_report_incident_sub, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mHolder = new ViewHolder(view);
        manipulateBundle(getArguments());

    }

    private void manipulateBundle(Bundle bundle) {
        if (bundle != null) {
            Titlename = bundle.getString("titlename");
            mHolder.textView_titlename.setText(Titlename);
            mHolder.ImageView_Camera.setOnClickListener(this);
            mHolder.imageView_Video.setOnClickListener(this);
            mHolder.imageView_Mic.setOnClickListener(this);
            if (getActivity() instanceof ToolbarListener) {
                ((ToolbarListener) getActivity()).setTitle("Islamabad Police");
            }
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ImageView_camera:
                showdialog();
                break;
            case R.id.ImageView_video:
                chooseVideoFromCamera();
                break;
            case R.id.ImageView_mic:
                chooseaudioFromGallary();
                break;

        }

    }


    private void showdialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("Please Select....");
        builder.setSingleChoiceItems(items, -1, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                switch (item) {
                    case 0:
                        choosePhotoFromGallary();
                        break;
                    case 1:
                        takePhotoFromCamera();
                        break;
                }
                levelDialog.dismiss();
            }
        });
        levelDialog = builder.create();
        levelDialog.show();
    }

    public static class ViewHolder {


        RecyclerView recycler_dashboard;
        TextView textView_titlename;
        Button button_submit;
        EditText editText_detail;
        ImageView ImageView_Camera, imageView_Video, imageView_Mic;

        public ViewHolder(View view) {
            recycler_dashboard = (RecyclerView) view.findViewById(R.id.recyclerView_dashboard);
            textView_titlename = (TextView) view.findViewById(R.id.txt_TitleName);
            button_submit = (Button) view.findViewById(R.id.Button_Submit);
            editText_detail = (EditText) view.findViewById(R.id.Edittext_Detail);
            ImageView_Camera = (ImageView) view.findViewById(R.id.ImageView_camera);
            imageView_Video = (ImageView) view.findViewById(R.id.ImageView_video);
            imageView_Mic = (ImageView) view.findViewById(R.id.ImageView_mic);

        }

    }

    private void chooseVideoFromCamera() {
        Intent intent = new Intent();
        intent.setType("video/*");
        intent.setAction(Intent.ACTION_PICK);
        startActivityForResult(Intent.createChooser(intent, "Select Video "), VIDEO);

    }

    public void chooseaudioFromGallary() {
        Intent intent = new Intent();
        intent.setType("*/*");
        intent.setAction(Intent.ACTION_PICK);
        startActivityForResult(Intent.createChooser(intent, "Select Audio "), MIC);
    }


    public void choosePhotoFromGallary() {
        Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(galleryIntent, GALLERY);
    }



    private void takePhotoFromCamera() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//
        // Ensure that there's a camera activity to handle the intent
        // Create the File where the photo should go
        File photoFile = null;
        try {
            photoFile = createImageFile();
        } catch (IOException ex) {
            // Error occurred while creating the File
        }
        // Continue only if the File was successfully created
        if (Checkpermission()) {

            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(getActivity(),
                        "com.hypernym.citizenapp.fileprovider",
                        photoFile);
                if ( Build.VERSION.SDK_INT <= Build.VERSION_CODES.LOLLIPOP ) {
                    takePictureIntent.addFlags( Intent.FLAG_GRANT_WRITE_URI_PERMISSION|Intent.FLAG_GRANT_READ_URI_PERMISSION );
                    takePictureIntent.setClipData( ClipData.newRawUri( MediaStore.EXTRA_OUTPUT, photoURI ) );
                    startActivityForResult(takePictureIntent, CAMERAA);
                }
                else {
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                    startActivityForResult(takePictureIntent, CAMERAA);
                }

            }

        } else {
            AppUtils.showSnackBar(getView(), AppUtils.getErrorMessage(getContext(), 11));
            requestlocationpermission();
        }

    }


    private void galleryAddPic() {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        File f = new File(mCurrentPhotoPath);
        file_name = f;
        Uri contentUri = Uri.fromFile(f);
        mediaScanIntent.setData(contentUri);
        getActivity().sendBroadcast(mediaScanIntent);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == getActivity().RESULT_CANCELED) {
            return;
        }
        if (requestCode == GALLERY) {
            if (data != null) {
                mSelectedProfileImageURI = data.getData();
                try {
                    //thumbnail = MediaStore.Images.Media.getBitmap(getContext().getContentResolver(), mSelectedProfileImageURI);
                    mProfileImageDecodableString = AppUtils.getRealPathFromURI(getContext(), mSelectedProfileImageURI);

                    if (mProfileImageDecodableString == null) {
                        AppUtils.showSnackBar(getView(), getString(R.string.err_image));
                        return;
                    }
                    mIsProfileImageAdded = true;
                    if (mIsProfileImageAdded) {
                        //File image = AppUtils.readFromConfigFile();
                        file_name = new File(AppUtils.getRealPathFromURI(getContext(), mSelectedProfileImageURI));
                        globalImagePath = file_name.getAbsolutePath();
                        RequestBody reqFile = RequestBody.create(MediaType.parse("image/*"), file_name);
                        imgName = file_name.getName();
//                        img_title.setImageBitmap(loadFromFile(globalImagePath));
//                        mHolder.img_cross.setVisibility(View.VISIBLE);
                        // img_title.setImageBitmap(getThumbnail(mProfileImageDecodableString));
                        //txt_img_name.setText(image.getName());
                        partImage = MultipartBody.Part.createFormData("photo", file_name.getName(), reqFile);
                        Toast.makeText(getContext(), "Image Saved from Gallery!", Toast.LENGTH_SHORT).show();
                        AppUtils.showSnackBar(getView(), AppUtils.getErrorMessage(getContext(), 7));

                        if (file_name.length() / Constants.ONE_THOUSAND_AND_TWENTY_FOUR > Constants.IMAGE_SIZE_IN_KB) {
                            AppUtils.showSnackBar(getView(), getString(R.string.err_image_size_large, Constants.IMAGE_SIZE_IN_KB));
                            //   dialog.dismiss();
                            return;
                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    // Toast.makeText(getContext(), "Failed!", Toast.LENGTH_SHORT).show();
                    AppUtils.showSnackBar(getView(), AppUtils.getErrorMessage(getContext(), 2));

                }
            }

        }
        //      }
        else if (requestCode == CAMERAA) {

            mIsProfileImageAdded = true;
            galleryAddPic();
            RequestBody reqFile = RequestBody.create(MediaType.parse("image/*"), file_name);
            imgName = file_name.getName();
            globalImagePath = file_name.getAbsolutePath();
            //txt_img_name.setText(file_name.getName());
            // img_title.setImageBitmap(loadFromFile(globalImagePath));
            Bitmap orignal = loadFromFile(globalImagePath);
            File filenew = new File(globalImagePath);
            try {
                FileOutputStream out = new FileOutputStream(filenew);
                orignal.compress(Bitmap.CompressFormat.JPEG, 50, out);
                out.flush();
                out.close();

            } catch (Exception e) {
                e.printStackTrace();
            }
            //  mHolder.img_cross.setVisibility(View.VISIBLE);
            partImage = MultipartBody.Part.createFormData("photo", file_name.getName(), reqFile);
            Toast.makeText(getContext(), "Image Saved from Camera!", Toast.LENGTH_SHORT).show();
           // AppUtils.showSnackBar(getView(), AppUtils.getErrorMessage(getContext(), 8));
            if (file_name.length() / Constants.ONE_THOUSAND_AND_TWENTY_FOUR > Constants.IMAGE_SIZE_IN_KB) {
                AppUtils.showSnackBar(getView(), getString(R.string.err_image_size_large, Constants.IMAGE_SIZE_IN_KB));
                //   dialog.dismiss();
                return;
            }
        } else if (requestCode == VIDEO) {
            //     AppUtils.showSnackBar(getView(), getString(R.string.err_image_not_selected));
            if (data != null) {
                mSelectedProfileImageURI = data.getData();
                try {
                    mProfileImageDecodableString = FileUtils.getPath(getContext(), mSelectedProfileImageURI);
                    String x = "";

                    if (mProfileImageDecodableString == null) {
                        AppUtils.showSnackBar(getView(), getString(R.string.err_image));
                        return;
                    }
                    mIsProfileImageAdded = true;
                    if (mIsProfileImageAdded) {
                        file_name = new File(AppUtils.getRealPathFromURI(getContext(), mSelectedProfileImageURI));
                        globalImagePath = file_name.getAbsolutePath();
                        RequestBody reqFile = RequestBody.create(MediaType.parse("*/*"), file_name);
                        imgName = file_name.getName();
                        partImage = MultipartBody.Part.createFormData("photo", file_name.getName(), reqFile);
                        Toast.makeText(getContext(), "Video Saved from Gallery!", Toast.LENGTH_SHORT).show();
                      //  AppUtils.showSnackBar(getView(), AppUtils.getErrorMessage(getContext(), 7));
                        if (file_name.length() / Constants.ONE_THOUSAND_AND_TWENTY_FOUR > Constants.IMAGE_SIZE_IN_KB) {
                            AppUtils.showSnackBar(getView(), getString(R.string.err_image_size_large, Constants.IMAGE_SIZE_IN_KB));
                            return;
                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    // Toast.makeText(getContext(), "Failed!", Toast.LENGTH_SHORT).show();
                    AppUtils.showSnackBar(getView(), AppUtils.getErrorMessage(getContext(), 2));

                }
            }
        } else {
            if (data != null) {
                mSelectedProfileImageURI = data.getData();
                try {
                    //thumbnail = MediaStore.Images.Media.getBitmap(getContext().getContentResolver(), mSelectedProfileImageURI);

                    mProfileImageDecodableString = FileUtils.getPath(getContext(), mSelectedProfileImageURI);
                    //  mProfileImageDecodableString=  data.getData().getPath();

//                    File file=new File(new URI(mProfileImageDecodableString));
                    String x = "";

                    if (mProfileImageDecodableString == null) {
                        AppUtils.showSnackBar(getView(), getString(R.string.err_image));
                        return;
                    }
                    mIsProfileImageAdded = true;
                    if (mIsProfileImageAdded) {
                        //File image = AppUtils.readFromConfigFile();
                        file_name = new File(AppUtils.getRealPathFromURI(getContext(), mSelectedProfileImageURI));
                        globalImagePath = file_name.getAbsolutePath();
                        RequestBody reqFile = RequestBody.create(MediaType.parse("*/*"), file_name);
                        imgName = file_name.getName();
//                        img_title.setImageBitmap(loadFromFile(globalImagePath));
//                        mHolder.img_cross.setVisibility(View.VISIBLE);
                        // img_title.setImageBitmap(getThumbnail(mProfileImageDecodableString));
                        //txt_img_name.setText(image.getName());
                        partImage = MultipartBody.Part.createFormData("photo", file_name.getName(), reqFile);
                        Toast.makeText(getContext(), "Audio Saved from Gallery!", Toast.LENGTH_SHORT).show();
                      //  AppUtils.showSnackBar(getView(), AppUtils.getErrorMessage(getContext(), 7));

                        if (file_name.length() / Constants.ONE_THOUSAND_AND_TWENTY_FOUR > Constants.IMAGE_SIZE_IN_KB) {
                            AppUtils.showSnackBar(getView(), getString(R.string.err_image_size_large, Constants.IMAGE_SIZE_IN_KB));
                            //   dialog.dismiss();
                            return;
                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    // Toast.makeText(getContext(), "Failed!", Toast.LENGTH_SHORT).show();
                    AppUtils.showSnackBar(getView(), AppUtils.getErrorMessage(getContext(), 2));

                }
            }
        }
    }


    public static Bitmap loadFromFile(String filename) {
        try {
            File f = new File(filename);
            if (!f.exists()) {
                return null;
            }
            Bitmap tmp = BitmapFactory.decodeFile(filename);
            return tmp;
        } catch (Exception e) {
            return null;
        }
    }


    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getContext().getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }


    public boolean Checkpermission() {

        int CameraPermissionResult = ContextCompat.checkSelfPermission(getActivity(), CAMERA);


        return CameraPermissionResult==PackageManager.PERMISSION_GRANTED;
    }
    private void requestlocationpermission() {
        ActivityCompat.requestPermissions(getActivity(), new String[]
                {
                        ACCESS_FINE_LOCATION,
                        READ_EXTERNAL_STORAGE,
                        CALL_PHONE,
                        CAMERA


                }, RequestPermissionCode);


    }
    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {

            case RequestPermissionCode:

                if (grantResults.length > 0) {


                    boolean camerpermission = grantResults[2] == PackageManager.PERMISSION_GRANTED;

                    if (camerpermission) {
                        // Toast.makeText(HomeActivity.this, "Permission Granted", Toast.LENGTH_LONG).show();
                    } else {
                        //Toast.makeText(HomeActivity.this,"Permission Denied",Toast.LENGTH_LONG).show();
                    }
                }

                break;
        }
    }


}