package com.hypernym.citizenapp.fragments;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.hypernym.citizenapp.R;
import com.hypernym.citizenapp.toolbox.ToolbarListener;
import com.hypernym.citizenapp.utils.AppUtils;

public class ProfileFragment extends Fragment implements View.OnClickListener {

    private ViewHolder mHolder;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
//        if (!EventBus.getDefault().isRegistered(this))
//            EventBus.getDefault().register(this);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof ToolbarListener) {
            ((ToolbarListener) context).setTitle("");
        }
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_profile, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        AppUtils.hideKeyboard(getActivity());
        mHolder = new ProfileFragment.ViewHolder(view);
    }


    @Override
    public void onClick(View view) {

    }

    public static class ViewHolder {
        TextView textViewPolice,textViewStation,textViewInfo;

        public ViewHolder(View view) {
            textViewPolice=(TextView)view.findViewById(R.id.lbl_police);
            textViewStation=(TextView)view.findViewById(R.id.lbl_stations);
            textViewInfo=(TextView)view.findViewById(R.id.lbl_info);
            Typeface poppinsRegular = Typeface.createFromAsset(textViewPolice.getContext().getAssets(), "Poppins-Regular.ttf");
            Typeface poppinsmedium = Typeface.createFromAsset(textViewStation.getContext().getAssets(), "Poppins-Medium.ttf");
            textViewPolice.setTypeface(poppinsRegular);
            textViewStation.setTypeface(poppinsmedium);
            textViewInfo.setTypeface(poppinsmedium);
        }

    }
}
