package com.hypernym.citizenapp.fragments;

import android.app.DownloadManager;
import android.content.Context;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.github.rahatarmanahmed.cpv.CircularProgressView;
import com.hypernym.citizenapp.R;
import com.hypernym.citizenapp.adapter.DownloadAdapter;
import com.hypernym.citizenapp.adapter.ImportantNumberAdapter;
import com.hypernym.citizenapp.api.ApiInterface;
import com.hypernym.citizenapp.model.DownloadData;
import com.hypernym.citizenapp.model.DownloadList;
import com.hypernym.citizenapp.toolbox.OnItemClickListener;
import com.hypernym.citizenapp.toolbox.ToolbarListener;
import com.hypernym.citizenapp.utils.AppUtils;
import com.hypernym.citizenapp.utils.Constants;
import com.hypernym.citizenapp.utils.ItemDecorationAlbumColumns;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DownloadPdfFragment extends Fragment implements View.OnClickListener, OnItemClickListener {

    private ViewHolder mHolder;
    List<DownloadList> downloadLists;
    DownloadAdapter downloadAdapter;
    RecyclerView.LayoutManager layoutManager;
    DownloadManager downloadManager;
    Uri uri;



    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
//        if (!EventBus.getDefault().isRegistered(this))
//            EventBus.getDefault().register(this);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof ToolbarListener) {
            ((ToolbarListener) context).setTitle("");
        }
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_download, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mHolder = new ViewHolder(view);
        AppUtils.hideKeyboard(getActivity());
        mHolder.circularProgressView.startAnimation();
        mHolder.circularProgressView.setVisibility(View.VISIBLE);
        downloadManager=(DownloadManager)getActivity().getSystemService(Context.DOWNLOAD_SERVICE);

        mHolder.Recycler_Download.setHasFixedSize(true);
        mHolder.Recycler_Download.setLayoutManager(new GridLayoutManager(getContext(), 1));
        mHolder.Recycler_Download.addItemDecoration(new ItemDecorationAlbumColumns(
                getResources().getDimensionPixelSize(R.dimen.spacing_large), 1));

        // 3. create an adapter



        gettingdata();

    }


    @Override
    public void onClick(View view) {

    }

    public void gettingdata() {
        ApiInterface.retrofit.getdownloadlist().enqueue(new Callback<DownloadData>() {
            @Override
            public void onResponse(Call<DownloadData> call, Response<DownloadData> response) {
                try {
                    if (response.isSuccessful()) {
                        mHolder.circularProgressView.stopAnimation();
                        mHolder.circularProgressView.setVisibility(View.GONE);
                        downloadLists = response.body().getData();
                        RecyclerSetup();
                    }
                } catch (Exception ex) {
                    mHolder.circularProgressView.stopAnimation();
                    mHolder.circularProgressView.setVisibility(View.GONE);
                    AppUtils.showSnackBar(getView(), AppUtils.getErrorMessage(getContext(), 2));
                }
            }

            @Override
            public void onFailure(Call<DownloadData> call, Throwable t) {
                AppUtils.showSnackBar(getView(), AppUtils.getErrorMessage(getContext(), Constants.NETWORK_ERROR));
                mHolder.circularProgressView.stopAnimation();
                mHolder.circularProgressView.setVisibility(View.GONE);

            }
        });

    }

    private void RecyclerSetup() {
//        downloadAdapter = new DownloadAdapter(downloadLists, this, getContext());
//        mHolder.Recycler_Download.setAdapter(downloadAdapter);

        downloadAdapter = new DownloadAdapter(downloadLists, this, getContext());
        // 4. set adapter
        mHolder.Recycler_Download.setAdapter(downloadAdapter);
        // 5. set item animator to DefaultAnimator
        mHolder.Recycler_Download.setItemAnimator(new DefaultItemAnimator());
    }

    @Override
    public void onItemClick(View view, Object data, int position) {
        switch (view.getId())
        {
            case R.id.item_layout:
                if(downloadLists.get(position).getUrl()!=null)
                {
                    uri=Uri.parse(downloadLists.get(position).getUrl());
                    DownloadManager.Request request=new DownloadManager.Request(uri);
                    request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
                    Long refernace=downloadManager.enqueue(request);
                }
                else {
                    Toast.makeText(getContext(), "No File To Download", Toast.LENGTH_SHORT).show();
                }
                break;
        }


    }

    public static class ViewHolder {


        RecyclerView Recycler_Download;
        CircularProgressView circularProgressView;
        TextView textViewImportant,textViewDocument;

        public ViewHolder(View view) {
            Recycler_Download = (RecyclerView) view.findViewById(R.id.recyclerView_download);
            circularProgressView = (CircularProgressView) view.findViewById(R.id.progress);
            textViewImportant=(TextView)view.findViewById(R.id.lbl_important);
            textViewDocument=(TextView)view.findViewById(R.id.lbl_documents);
            Typeface poppinsRegular = Typeface.createFromAsset(textViewImportant.getContext().getAssets(), "Poppins-Regular.ttf");
            Typeface poppinsmedium = Typeface.createFromAsset(textViewDocument.getContext().getAssets(), "Poppins-Medium.ttf");
            textViewImportant.setTypeface(poppinsRegular);
            textViewDocument.setTypeface(poppinsmedium);

        }

    }
}