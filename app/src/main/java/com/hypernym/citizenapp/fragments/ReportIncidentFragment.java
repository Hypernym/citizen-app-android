package com.hypernym.citizenapp.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.hypernym.citizenapp.FrameActivity;
import com.hypernym.citizenapp.R;
import com.hypernym.citizenapp.adapter.ReportIncidentAdapter;
import com.hypernym.citizenapp.model.ItemData;
import com.hypernym.citizenapp.toolbox.OnItemClickListener;
import com.hypernym.citizenapp.toolbox.ToolbarListener;
import com.hypernym.citizenapp.utils.ActivityUtils;
import com.hypernym.citizenapp.utils.ItemDecorationAlbumColumns;

public class ReportIncidentFragment extends Fragment implements View.OnClickListener,OnItemClickListener {

    private ViewHolder mHolder;
    ReportIncidentAdapter reportIncidentAdapter;
    Bundle bundle=new Bundle();
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
//        if (!EventBus.getDefault().isRegistered(this))
//            EventBus.getDefault().register(this);
    }
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof ToolbarListener) {
            ((ToolbarListener) context).setTitle("Report Incident");
        }
    }
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_report_incident, container, false);
    }
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mHolder = new ViewHolder(view);
        ItemData itemsData[] = {

                new ItemData("Accident", R.drawable.accident,""),
                new ItemData("Assaults/Abuse",R.drawable.abuse,""),
                new ItemData("Distrubance",R.drawable.disturbance,""),
                new ItemData("Drugs/Alcohol",R.drawable.drugs,""),
                new ItemData("Harassment",R.drawable.harrassment,""),
                new ItemData("Kidnapping",R.drawable.kidnapping,""),
                new ItemData("Traffic Violation",R.drawable.traffic_violation,""),
                new ItemData("Suspicious Activity",R.drawable.suspicious_activity,""),
                new ItemData("Theft",R.drawable.theft,""),
                new ItemData("Others",R.drawable.others,""),
                new ItemData("Courrption",R.drawable.corruption,""),
                new ItemData("Terrorism",R.drawable.terrorism,"")
        };
        RecyclerSetup(itemsData);




    }

    private void RecyclerSetup(ItemData[] itemData) {
        mHolder.recycler_report_incident.setHasFixedSize(true);
        mHolder.recycler_report_incident.setLayoutManager(new GridLayoutManager(getContext(), 3));
        mHolder.recycler_report_incident.addItemDecoration(new ItemDecorationAlbumColumns(
                getResources().getDimensionPixelSize(R.dimen.spacing_medium), 3));
        reportIncidentAdapter = new ReportIncidentAdapter(itemData,this,getContext());
        mHolder.recycler_report_incident.setAdapter(reportIncidentAdapter);
        mHolder.recycler_report_incident.setItemAnimator(new DefaultItemAnimator());
    }
    @Override
    public void onClick(View view) {

    }

    @Override
    public void onItemClick(View view, Object data, int position) {
        if(position==0)
        {
            bundle.putString("titlename","Accident");
            ActivityUtils.startActivity(getContext(),FrameActivity.class,ReportIncidentSubFragment.class.getName(),bundle,false);

        }
        else if(position==1)
        {
            bundle.putString("titlename","Assaults/Abuse");
            ActivityUtils.startActivity(getContext(),FrameActivity.class,ReportIncidentSubFragment.class.getName(),bundle,false);

        }

    }

    public static class ViewHolder {
        RecyclerView recycler_report_incident;
        public ViewHolder(View view) {
            recycler_report_incident = (RecyclerView) view.findViewById(R.id.recyclerView_ReportIncident);

        }

    }
}