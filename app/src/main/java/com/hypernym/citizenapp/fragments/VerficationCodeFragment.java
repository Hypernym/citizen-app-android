package com.hypernym.citizenapp.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.github.rahatarmanahmed.cpv.CircularProgressView;
import com.hypernym.citizenapp.LoginActivity;
import com.hypernym.citizenapp.R;
import com.hypernym.citizenapp.SignUpActivity;
import com.hypernym.citizenapp.SimpleFrameActivity;
import com.hypernym.citizenapp.api.ApiInterface;
import com.hypernym.citizenapp.enumerations.AnimationEnum;
import com.hypernym.citizenapp.model.WebAPIResponse;
import com.hypernym.citizenapp.toolbox.ToolbarListener;
import com.hypernym.citizenapp.utils.ActivityUtils;
import com.hypernym.citizenapp.utils.AppUtils;
import com.hypernym.citizenapp.utils.Constants;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class VerficationCodeFragment extends Fragment implements View.OnClickListener {

    private ViewHolder mHolder;
    String ID;
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
//        if (!EventBus.getDefault().isRegistered(this))
//            EventBus.getDefault().register(this);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof ToolbarListener) {
            ((ToolbarListener) context).setTitle("Verification");
        }
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_verfication_code, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mHolder = new ViewHolder(view);
        mHolder.button_Proceed.setOnClickListener(this);

        manipulateBundle(getArguments());
    }

    private void manipulateBundle(Bundle bundle) {

        if (bundle != null) {
//            mHolder.textInputEditText_code.setText(""+bundle.getString("id"));
            ID=bundle.getString("id");
        }
    }


    @Override
    public void onClick(View view) {
        switch (view.getId())
        {
            case R.id.button_proceed:
                verificationCodeCheck();
                break;
        }

    }

    private void verificationCodeCheck() {
        mHolder.circularProgressView.startAnimation();
        mHolder.circularProgressView.setVisibility(View.VISIBLE);
        HashMap<String, Object> body = new HashMap<>();
        body.put("id",ID);
        body.put("activation_code",mHolder.textInputEditText_code.getText().toString());

        ApiInterface.retrofit.verify_code(body).enqueue(new Callback<WebAPIResponse<String>>() {
            @Override
            public void onResponse(Call<WebAPIResponse<String>> call, Response<WebAPIResponse<String>> response) {
                try {
                    if (response.body().condition.equals("success")) {
                        mHolder.circularProgressView.stopAnimation();
                        mHolder.circularProgressView.setVisibility(View.GONE);
                        ActivityUtils.startActivity(getActivity(), LoginActivity.class, true);
                    }
                    else
                    {
                        mHolder.circularProgressView.stopAnimation();
                        mHolder.circularProgressView.setVisibility(View.GONE);
                        AppUtils.showSnackBar(getView(), ""+response.body().error_msg);
                    }
                } catch (Exception ex) {
                    AppUtils.showSnackBar(getView(), AppUtils.getErrorMessage(getContext(), 2));
                    mHolder.circularProgressView.stopAnimation();
                    mHolder.circularProgressView.setVisibility(View.GONE);

                }
            }

            @Override
            public void onFailure(Call<WebAPIResponse<String>> call, Throwable t) {
                AppUtils.showSnackBar(getView(), AppUtils.getErrorMessage(getContext(), Constants.NETWORK_ERROR));
                mHolder.circularProgressView.stopAnimation();
                mHolder.circularProgressView.setVisibility(View.GONE);
            }
        });


    }

    public static class ViewHolder {
        TextInputEditText textInputEditText_code;
        Button button_Proceed;
        CircularProgressView circularProgressView;

        public ViewHolder(View view) {
            
            textInputEditText_code = (TextInputEditText) view.findViewById(R.id.edit_text_code);
            button_Proceed = (Button) view.findViewById(R.id.button_proceed);
            circularProgressView = (CircularProgressView) view.findViewById(R.id.progress);
        }

    }
}