package com.hypernym.citizenapp;

import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ProgressBar;

import com.hypernym.citizenapp.api.ApiInterface;
import com.hypernym.citizenapp.model.WebAPIResponse;
import com.hypernym.citizenapp.utils.AppUtils;

import java.util.HashMap;

import fr.ganfra.materialspinner.MaterialSpinner;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SignUpActivity extends AppCompatActivity implements View.OnClickListener, MaterialSpinner.OnItemSelectedListener {
    Button btn_SignUp;
    private TextInputEditText edit_username, edit_fullname, edit_cnic_number, edit_email, edit_mobile,
            edit_confirm_password, edit_current_address, edit_permanent_address, edit_password;
    private TextInputLayout inputLayout_username, inputLayout_password, inputLayout_fullname, inputLayout_cnic_number,
            inputLayout_email, inputLayout_mobile, inputLayout_confirm_password, inputLayout_current_address, inputLayout_permanent_address;
    ProgressBar circularProgressView;
    String Spinner_Item = "Male";
    MaterialSpinner spinner;
    FrameLayout frameLayout;
    String[] arraySpinner = new String[]{"Male", "Female"};
    Object item;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        AppUtils.hideKeyboard(this);
        btn_SignUp = (Button) findViewById(R.id.button_SignUp);
        spinner = (MaterialSpinner) findViewById(R.id.spinner_signup);
        frameLayout = (FrameLayout) findViewById(R.id.rootview);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, arraySpinner);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(this);
        // btn_SignUp = (Button) findViewById(R.id.sign_up);
        //   inputLayout_username = (TextInputLayout) findViewById(R.id.textInputLayoutUsername);
        inputLayout_password = (TextInputLayout) findViewById(R.id.textInputLayoutPassword);
        inputLayout_fullname = (TextInputLayout) findViewById(R.id.textInputLayoutFullname);
        inputLayout_cnic_number = (TextInputLayout) findViewById(R.id.textInputLayoutCnic);
        inputLayout_email = (TextInputLayout) findViewById(R.id.textInputLayoutEmail);
        inputLayout_mobile = (TextInputLayout) findViewById(R.id.textInputLayoutMobile);
        inputLayout_confirm_password = (TextInputLayout) findViewById(R.id.textInputLayoutConfimPassword);
        // inputLayout_current_address = (TextInputLayout) findViewById(R.id.textInputLayoutCurrentAddress);
        // inputLayout_permanent_address = (TextInputLayout) findViewById(R.id.textInputLayoutPermanentAddress);
        circularProgressView = (ProgressBar) findViewById(R.id.progress);

        // edit_username = (TextInputEditText) findViewById(R.id.editTextUsername);
        edit_password = (TextInputEditText) findViewById(R.id.editTextPassword);
        edit_fullname = (TextInputEditText) findViewById(R.id.editTextFullname);
        edit_cnic_number = (TextInputEditText) findViewById(R.id.editTextCnic);
        edit_email = (TextInputEditText) findViewById(R.id.editTextEmail);
        edit_mobile = (TextInputEditText) findViewById(R.id.editTextMobile);
        edit_confirm_password = (TextInputEditText) findViewById(R.id.editTextConfirmPassword);
        // edit_current_address = (TextInputEditText) findViewById(R.id.editTextCurrentAddress);
        // edit_permanent_address = (TextInputEditText) findViewById(R.id.editTextPermanentAddress);
        //  edit_username.addTextChangedListener(new MyTextWatcher(edit_username));
        edit_password.addTextChangedListener(new MyTextWatcher(edit_password));
        edit_confirm_password.addTextChangedListener(new MyTextWatcher(edit_confirm_password));
        edit_password.addTextChangedListener(new MyTextWatcher(edit_password));
        edit_fullname.addTextChangedListener(new MyTextWatcher(edit_fullname));
        edit_email.addTextChangedListener(new MyTextWatcher(edit_email));
        edit_mobile.addTextChangedListener(new MyTextWatcher(edit_mobile));

        btn_SignUp.setOnClickListener(this);


        //    progressBar.setVisibility(View.GONE);

    }


    private void SignUp() {
        if (!validateFullname())
            return;
//        if (!validateName())
//            return;
//        if (!validateCnic())
//            return;
        if (!validateEmail())
            return;
        if (!validateMobile())
            return;
        if (!validatePassword())
            return;
        if (!ValidationPasswordCheck())
            return;
//        if (!validateCurrentAddress())
//            return;
//        if (!validatePermanentAddress())
//            return;
        circularProgressView.setVisibility(View.VISIBLE);
        HashMap<String, Object> body = new HashMap<>();
        body.put("full_name", edit_fullname.getText().toString());
        //  body.put("username", edit_username.getText().toString());
//        body.put("cnic", edit_cnic_number.getText().toString());
        body.put("email", edit_email.getText().toString());
        body.put("mobile", edit_mobile.getText().toString());
        body.put("gender", Spinner_Item);
        body.put("password", edit_password.getText().toString());
//        body.put("current_address", edit_current_address.getText().toString());
//        body.put("permanent_address", edit_permanent_address.getText().toString());


        ApiInterface.retrofit.registeruser(body).enqueue(new Callback<WebAPIResponse>() {
            @Override
            public void onResponse(Call<WebAPIResponse> call, Response<WebAPIResponse> response) {
                try {
                    if (response.isSuccessful()) {

                        circularProgressView.setVisibility(View.GONE);
                        Bundle bundle = new Bundle();
                        bundle.putString("id", String.valueOf(response.body().data));
                        //  finish();
                    } else {
                        AppUtils.showSnackBar(frameLayout, "" + response.body().error_msg);
                    }
                } catch (Exception ex) {

                    circularProgressView.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailure(Call<WebAPIResponse> call, Throwable t) {
                circularProgressView.setVisibility(View.GONE);
//                mHolder.circularProgressView.stopAnimation();
//                mHolder.circularProgressView.setVisibility(View.GONE);
            }
        });

    }

    private boolean validateName() {
        if (edit_username.getText().toString().trim().isEmpty()) {
            inputLayout_username.setError(getString(R.string.err_msg_name));
            requestFocus(edit_username);
            return false;
        } else {
            inputLayout_username.setErrorEnabled(false);
        }

        return true;
    }

    private boolean validatePassword() {
        if (edit_password.getText().toString().trim().isEmpty()) {
            inputLayout_password.setError(getString(R.string.err_msg_password));
            requestFocus(edit_password);
            return false;
        } else {
            inputLayout_password.setErrorEnabled(false);
        }
        return true;
    }

    private boolean validateFullname() {
        if (edit_fullname.getText().toString().trim().isEmpty()) {
            inputLayout_fullname.setError(getString(R.string.err_msg_fullname));
            requestFocus(edit_fullname);
            return false;
        } else {
            inputLayout_fullname.setErrorEnabled(false);
        }
        return true;
    }

    private boolean validateCnic() {
        if (edit_cnic_number.getText().toString().trim().isEmpty()) {
            inputLayout_cnic_number.setError(getString(R.string.err_msg_cnic));
            requestFocus(edit_cnic_number);
            return false;
        } else {
            inputLayout_cnic_number.setErrorEnabled(false);
        }
        return true;
    }

    private boolean validateMobile() {
        if (edit_mobile.getText().toString().trim().isEmpty()) {
            inputLayout_mobile.setError(getString(R.string.err_msg_mobile));
            requestFocus(edit_mobile);
            return false;
        } else {
            inputLayout_mobile.setErrorEnabled(false);
        }
        return true;
    }

    private boolean validatePermanentAddress() {
        if (edit_permanent_address.getText().toString().trim().isEmpty()) {
            inputLayout_permanent_address.setError(getString(R.string.err_msg_permanentaddress));
            requestFocus(edit_permanent_address);
            return false;
        } else {
            inputLayout_permanent_address.setErrorEnabled(false);
        }
        return true;
    }

    private boolean validateCurrentAddress() {
        if (edit_current_address.getText().toString().trim().isEmpty()) {
            inputLayout_current_address.setError(getString(R.string.err_msg_currentaddress));
            requestFocus(edit_current_address);
            return false;
        } else {
            inputLayout_current_address.setErrorEnabled(false);
        }
        return true;
    }

    private boolean validateEmail() {
        if (edit_email.getText().toString().trim().isEmpty()) {
            inputLayout_email.setError(getString(R.string.err_msg_email));
            requestFocus(edit_email);
            return false;
        } else {
            inputLayout_email.setErrorEnabled(false);
        }
        return true;
    }

    private boolean validateConfirmPassword() {
        if (edit_confirm_password.getText().toString().trim().isEmpty()) {
            inputLayout_confirm_password.setError(getString(R.string.err_msg_confirmpassword));
            requestFocus(edit_confirm_password);
            return false;
        } else {
            inputLayout_confirm_password.setErrorEnabled(false);
        }
        return true;
    }


    private static boolean isValidEmail(String email) {
        return !TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button_SignUp:
                SignUp();
                break;
        }

    }


    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        switch (parent.getId()) {
            case R.id.spinner:
                item = parent.getItemAtPosition(position);
                if (item.equals("Male")) {
                    Spinner_Item = "Male";
                } else {
                    Spinner_Item = "Female";
                }
                break;
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }


    private class MyTextWatcher implements TextWatcher {
        private View view;

        private MyTextWatcher(View view) {
            this.view = view;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            switch (view.getId()) {
                case R.id.editTextUsername:
                    validateName();
                    break;
                case R.id.editTextPassword:
                    validatePassword();
                    break;
                case R.id.editTextFullname:
                    validateFullname();
                    break;
                case R.id.editTextCnic:
                    validateCnic();
                    break;
                case R.id.editTextConfirmPassword:
                    ValidationPasswordCheck();
                    break;
                case R.id.editTextEmail:
                    validateEmail();
                    break;
//                case R.id.editTextCurrentAddress:
//                    validateCurrentAddress();
//                    break;
//                case R.id.editTextPermanentAddress:
//                    validatePermanentAddress();
//                    break;
                case R.id.editTextMobile:
                    validateMobile();
                    break;
            }

        }
    }

    private Boolean ValidationPasswordCheck() {

        if (!edit_password.getText().toString().equals(edit_confirm_password.getText().toString())) {
            inputLayout_confirm_password.setError(getString(R.string.confirm_password));
            requestFocus(edit_confirm_password);
            //  Toast.makeText(this,  "password did not match!", Toast.LENGTH_SHORT).show();
            return false;
//            Toast.makeText(this, "matches", Toast.LENGTH_SHORT).show();
        } else {
            inputLayout_confirm_password.setErrorEnabled(false);
        }
        return true;
    }

}



