package com.hypernym.citizenapp.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.hypernym.citizenapp.R;
import com.hypernym.citizenapp.model.ImportantNumberList;
import com.hypernym.citizenapp.model.ItemData;
import com.hypernym.citizenapp.toolbox.OnItemClickListener;

import java.util.ArrayList;
import java.util.List;

public class ImportantNumberAdapter extends RecyclerView.Adapter<ImportantNumberAdapter.ViewHolder> {

    Context context;
    private OnItemClickListener itemClickListener;
    List<ImportantNumberList> importantNumberLists;
    String orig;
    public ImportantNumberAdapter(List<ImportantNumberList> importantNumberLists, OnItemClickListener itemClickListener, Context context) {
        this.importantNumberLists = importantNumberLists;
        this.context = context;
        this.itemClickListener = itemClickListener;
    }

    @Override
    public ImportantNumberAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                                int viewType) {
        View itemLayoutView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_important_number, null);

        ImportantNumberAdapter.ViewHolder viewHolder = new ImportantNumberAdapter.ViewHolder(itemLayoutView);
        return viewHolder;
    }
    @Override
    public void onBindViewHolder(ImportantNumberAdapter.ViewHolder viewHolder, final int position) {


        viewHolder.txtViewTitle.setText(importantNumberLists.get(position).getName());
        viewHolder.txtViewNumber.setText(importantNumberLists.get(position).getNumber());
        String url=importantNumberLists.get(position).getIcon();

        if (url != null) {
            Glide.with(context).load(url).apply(new RequestOptions().placeholder(R.drawable.ic_launcher)).into(viewHolder.imgViewIcon);
        }

        if (importantNumberLists.get(position).getCanSms().equals(1)) {
            viewHolder.imageViewMessage.setVisibility(View.VISIBLE);
        } else {
            viewHolder.imageViewMessage.setVisibility(View.GONE);
        }
        viewHolder.imageViewCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                itemClickListener.onItemClick(view, importantNumberLists.get(position), position);
            }
        });
        viewHolder.imageViewMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                itemClickListener.onItemClick(view, importantNumberLists.get(position), position);
            }
        });

    }

    // inner class to hold a reference to each item of RecyclerView
    public static class ViewHolder extends RecyclerView.ViewHolder {

        public TextView txtViewTitle, txtViewNumber;
        public ImageView imgViewIcon, imageViewCall,imageViewMessage;

        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);
            txtViewTitle = (TextView) itemLayoutView.findViewById(R.id.item_title);
            txtViewNumber = (TextView) itemLayoutView.findViewById(R.id.item_number);
            imgViewIcon = (ImageView) itemLayoutView.findViewById(R.id.item_icon);
            imageViewCall = (ImageView) itemLayoutView.findViewById(R.id.item_call);
            imageViewMessage = (ImageView) itemLayoutView.findViewById(R.id.item_message);
            Typeface poppinsRegular = Typeface.createFromAsset(txtViewTitle.getContext().getAssets(), "Poppins-Medium.ttf");
            Typeface poppinsRegular1 = Typeface.createFromAsset(txtViewNumber.getContext().getAssets(), "Poppins-Medium.ttf");

            txtViewTitle.setTypeface(poppinsRegular);
            txtViewNumber.setTypeface(poppinsRegular1);

        }
    }

    public  void filterList(ArrayList<ImportantNumberList> filterdNames) {
        importantNumberLists = filterdNames;
        notifyDataSetChanged();
    }

    // Return the size of your itemsData (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return importantNumberLists.size();
    }

    @Override
    public long getItemId(int position) {

        int itemID;

        // orig will be null only if we haven't filtered yet:
        if (orig == null)
        {
            itemID = position;
        }
        else
        {
            itemID = orig.indexOf(String.valueOf(importantNumberLists.get(position)));
        }
        return itemID;
    }

}