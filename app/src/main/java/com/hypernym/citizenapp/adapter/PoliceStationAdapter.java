package com.hypernym.citizenapp.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.hypernym.citizenapp.R;
import com.hypernym.citizenapp.model.PoliceStationList;
import com.hypernym.citizenapp.toolbox.OnItemClickListener;

import java.util.List;

public class PoliceStationAdapter extends RecyclerView.Adapter<PoliceStationAdapter.ViewHolder> {
    private List<PoliceStationList> itemsData;
    Context context;
    private OnItemClickListener itemClickListener;

    public PoliceStationAdapter(List<PoliceStationList> itemsData, OnItemClickListener itemClickListener, Context context) {
        this.itemsData = itemsData;
        this.context = context;
        this.itemClickListener = itemClickListener;
    }

    @Override
    public PoliceStationAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                                int viewType) {
        View itemLayoutView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_police_list, null);

        PoliceStationAdapter.ViewHolder viewHolder = new PoliceStationAdapter.ViewHolder(itemLayoutView);
        return viewHolder;
    }
    @Override
    public void onBindViewHolder(PoliceStationAdapter.ViewHolder viewHolder, final int position) {
     //   AppUtils.trustEveryone();
        final String item = String.valueOf(itemsData.get(position));
        if(itemsData.get(position).getName()==null){
            viewHolder.txtViewTitle.setText("--");
        }
        else{
            viewHolder.txtViewTitle.setText(itemsData.get(position).getName());
        }
        if(itemsData.get(position).getPhoneNo() == null){
            viewHolder.txtViewNumber.setText("--");
        }
        else{
            viewHolder.txtViewNumber.setText(itemsData.get(position).getPhoneNo());

        }
        if(itemsData.get(position).getAddress() == null){
            viewHolder.textViewAddress.setText("--");
        }
        else{
            viewHolder.textViewAddress.setText(itemsData.get(position).getAddress());

        }

//        if (position == 3 || position == 7) {
//            viewHolder.imageViewMessage.setVisibility(View.VISIBLE);
//        } else {
//            viewHolder.imageViewMessage.setVisibility(View.GONE);
//        }
        viewHolder.imageViewCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                itemClickListener.onItemClick(view, item, position);
            }
        });
        viewHolder.imageViewMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                itemClickListener.onItemClick(view, item, position);
            }
        });
        String url = itemsData.get(position).getIcon();
        if (url != null) {
            Log.e("TAAAG", "" + url);
            Glide.with(context).load(url).apply(new RequestOptions().placeholder(R.drawable.ic_launcher_background)).into(viewHolder.imgViewIcon);
        }


    }

    // inner class to hold a reference to each item of RecyclerView
    public static class ViewHolder extends RecyclerView.ViewHolder {

        public TextView txtViewTitle, txtViewNumber,textViewAddress;
        public ImageView imgViewIcon, imageViewCall,imageViewMessage;

        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);
            txtViewTitle = (TextView) itemLayoutView.findViewById(R.id.item_title);
            txtViewNumber = (TextView) itemLayoutView.findViewById(R.id.item_number);
            textViewAddress = (TextView) itemLayoutView.findViewById(R.id.item_Address);
            imgViewIcon = (ImageView) itemLayoutView.findViewById(R.id.item_icon);
            imageViewCall = (ImageView) itemLayoutView.findViewById(R.id.item_call);
            imageViewMessage = (ImageView) itemLayoutView.findViewById(R.id.item_location);

            Typeface title = Typeface.createFromAsset(txtViewTitle.getContext().getAssets(), "Poppins-Medium.ttf");
            Typeface number = Typeface.createFromAsset(txtViewNumber.getContext().getAssets(), "Poppins-Medium.ttf");
            Typeface address = Typeface.createFromAsset(txtViewNumber.getContext().getAssets(), "Poppins-Medium.ttf");

            txtViewTitle.setTypeface(title);
            txtViewNumber.setTypeface(number);
            textViewAddress.setTypeface(address);
        }
    }


    // Return the size of your itemsData (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return itemsData.size();
    }
}