package com.hypernym.citizenapp.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.hypernym.citizenapp.R;
import com.hypernym.citizenapp.model.DownloadList;
import com.hypernym.citizenapp.toolbox.OnItemClickListener;
import com.hypernym.citizenapp.utils.AppUtils;

import java.util.List;

public class DownloadAdapter extends RecyclerView.Adapter<DownloadAdapter.ViewHolder> {
    private List<DownloadList> itemsData;
    Context context;
    private OnItemClickListener itemClickListener;

    public DownloadAdapter(List<DownloadList> itemsData, OnItemClickListener itemClickListener, Context context) {
        this.itemsData = itemsData;
        this.context = context;
        this.itemClickListener = itemClickListener;
    }

    @Override
    public DownloadAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,int viewType) {
        View itemLayoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_download, null);

        DownloadAdapter.ViewHolder viewHolder = new DownloadAdapter.ViewHolder(itemLayoutView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(DownloadAdapter.ViewHolder viewHolder, final int position) {
//        AppUtils.trustEveryone();
        final String item = String.valueOf(itemsData.get(position));
        viewHolder.txtViewTitle.setText(itemsData.get(position).getName());

        viewHolder.linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                itemClickListener.onItemClick(view, item, position);
            }
        });

        String url = itemsData.get(position).getIcon();
        if (url != null) {
            Log.e("TAAAG", "" + url);
            Glide.with(context).load(url).apply(new RequestOptions().placeholder(R.drawable.ic_launcher_background)).into(viewHolder.imgViewIcon);
        }

    }

    // inner class to hold a reference to each item of RecyclerView
    public static class ViewHolder extends RecyclerView.ViewHolder {

        TextView txtViewTitle, txtViewNumber;
        ImageView imgViewIcon, imageViewCall, imageViewMessage;
        LinearLayout linearLayout;

        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);
            txtViewTitle = (TextView) itemLayoutView.findViewById(R.id.item_title);
            imgViewIcon = (ImageView) itemLayoutView.findViewById(R.id.item_icon);
            linearLayout = (LinearLayout) itemLayoutView.findViewById(R.id.item_layout);

        }
    }


    // Return the size of your itemsData (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return itemsData.size();
    }
}