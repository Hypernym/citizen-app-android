package com.hypernym.citizenapp;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.hypernym.citizenapp.api.ApiInterface;
import com.hypernym.citizenapp.model.User;
import com.hypernym.citizenapp.model.WebAPIResponse;
import com.hypernym.citizenapp.utils.AppUtils;
import com.hypernym.citizenapp.utils.LoginUtils;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {
    Button btn_sign, btn_SignUp;
    private TextInputEditText edit_username, edit_password;
    private TextInputLayout inputLayout_username, inputLayout_password;
    private LinearLayout linearLayoutSignup;
    SharedPreferences pref;
    TextView textView_SignUp,textView_title,textView_subtitle,textView_forgot,textView_account;
    User user = new User();
    FrameLayout frameLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        AppUtils.hideKeyboard(this);
        btn_sign = (Button) findViewById(R.id.button_login);
        // btn_SignUp = (Button) findViewById(R.id.sign_up);
        inputLayout_username = (TextInputLayout) findViewById(R.id.textInputLayoutUsername);
        inputLayout_password = (TextInputLayout) findViewById(R.id.textInputLayoutPassword);
        textView_SignUp = (TextView) findViewById(R.id.txt_Signup);
        textView_title = (TextView) findViewById(R.id.txt_TitleName);
        textView_subtitle = (TextView) findViewById(R.id.txt_subTitle);
        textView_forgot = (TextView) findViewById(R.id.txt_forgotPassword);
        textView_account = (TextView) findViewById(R.id.txt_account);
        edit_username = (TextInputEditText) findViewById(R.id.editTextUsername);
        edit_password = (TextInputEditText) findViewById(R.id.editTextPassword);
        frameLayout = (FrameLayout) findViewById(R.id.rootview);
        linearLayoutSignup = (LinearLayout) findViewById(R.id.layout_signup);
        // progressBar = (ProgressBar) findViewById(R.id.loader);

        pref = getApplicationContext().getSharedPreferences("TAG", MODE_PRIVATE);
        textView_SignUp.setOnClickListener(this);

        //    progressBar.setVisibility(View.GONE);
        edit_username.addTextChangedListener(new MyTextWatcher(edit_username));
        edit_password.addTextChangedListener(new MyTextWatcher(edit_password));

        btn_sign.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Login();
            }
        });
        linearLayoutSignup.setOnClickListener(this);
        settingFont();
    }

    private void settingFont() {
        Typeface poppinsRegular = Typeface.createFromAsset(textView_SignUp.getContext().getAssets(), "Poppins-Regular.ttf");
        Typeface title = Typeface.createFromAsset(textView_title.getContext().getAssets(), "Poppins-Regular.ttf");
        Typeface subtitle = Typeface.createFromAsset(textView_subtitle.getContext().getAssets(), "Poppins-Regular.ttf");
        Typeface forgot = Typeface.createFromAsset(textView_forgot.getContext().getAssets(), "Poppins-Regular.ttf");
        Typeface account = Typeface.createFromAsset(textView_account.getContext().getAssets(), "Poppins-Regular.ttf");
        Typeface username = Typeface.createFromAsset(edit_username.getContext().getAssets(), "Poppins-Regular.ttf");
        Typeface password = Typeface.createFromAsset(edit_password.getContext().getAssets(), "Poppins-Regular.ttf");

        textView_SignUp.setTypeface(poppinsRegular);
       textView_title.setTypeface(title);
        textView_subtitle.setTypeface(subtitle);
        textView_forgot.setTypeface(poppinsRegular);
        textView_account.setTypeface(forgot);
        edit_username.setTypeface(account);
        edit_password.setTypeface(username);
    }


    private void Login() {
        if (!validateName())
            return;
        if (!validatePassword())
            return;


        String username = edit_username.getText().toString(); //    "driver@kotal.com"
        String password = edit_password.getText().toString();//    "hypernym123"

        HashMap<String, Object> body = new HashMap<>();
        body.put("username", username);
        body.put("password", password);


        ApiInterface.retrofit.verify_login(body).enqueue(new Callback<WebAPIResponse<User>>() {
            @Override
            public void onResponse(Call<WebAPIResponse<User>> call, Response<WebAPIResponse<User>> response) {
                try {
                    if (response.body().condition.equals("success")) {
                        LoginUtils.saveUserToken(LoginActivity.this, response.body().data.getToken());
                        LoginUtils.userLoggedIn(LoginActivity.this);
                        LoginUtils.saveUser(getApplicationContext(),response.body().data);
                        Intent intent = new Intent(LoginActivity.this, HomeActivity.class);
                        startActivity(intent);
                        finish();
                    } else {
                        AppUtils.showSnackBar(frameLayout, "" + response.body().error_msg);

                    }


                } catch (Exception ex) {
                    AppUtils.showSnackBar(frameLayout, "" + response.body().error_msg);

                }
            }

            @Override
            public void onFailure(Call<WebAPIResponse<User>> call, Throwable t) {

                Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), "Establish Network Connection!", Snackbar.LENGTH_LONG);
                View view = snackbar.getView();
                AppUtils.showSnackBar(frameLayout, "Establish Network Connection!");

            }
        });

    }

    private boolean validateName() {
        if (edit_username.getText().toString().trim().isEmpty()) {
            inputLayout_username.setError(getString(R.string.err_msg_name));
            requestFocus(edit_username);
            return false;
        } else {
            inputLayout_username.setErrorEnabled(false);
        }

        return true;
    }

    private boolean validatePassword() {
        if (edit_password.getText().toString().trim().isEmpty()) {
            inputLayout_password.setError(getString(R.string.err_msg_password));
            requestFocus(edit_password);
            return false;
        } else {
            inputLayout_password.setErrorEnabled(false);
        }
        return true;
    }

    private static boolean isValidEmail(String email) {
        return !TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.layout_signup:
                Intent intent = new Intent(LoginActivity.this, SignUpActivity.class);
                startActivity(intent);
                finish();
                break;
        }

    }


    private class MyTextWatcher implements TextWatcher {
        private View view;

        private MyTextWatcher(View view) {
            this.view = view;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            switch (view.getId()) {
                case R.id.editTextUsername:
                    validateName();
                    break;
                case R.id.editTextPassword:
                    validatePassword();
                    break;
            }

        }
    }

}



