package com.hypernym.citizenapp.api;

import com.hypernym.citizenapp.R;
import com.hypernym.citizenapp.model.DownloadData;
import com.hypernym.citizenapp.model.ImportantNumberData;
import com.hypernym.citizenapp.model.PoliceStationData;
import com.hypernym.citizenapp.model.User;
import com.hypernym.citizenapp.model.VerificationCode;
import com.hypernym.citizenapp.model.WebAPIResponse;
import com.hypernym.citizenapp.toolbox.MyApplication;
import com.hypernym.citizenapp.utils.LoginUtils;

import java.io.IOException;
import java.io.InputStream;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.Arrays;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface ApiInterface {


   //String HTTP = "http://159.65.7.152/";//staging
    //String HTTP = "http://188.166.226.185/";//proud
    //  String HTTP = "http://192.168.2.131:8000/";//taimoor
  //  String HTTP = "http://192.168.2.108:8000/";//usman
    //  String HTTP = "http://192.168.2.195:8000/";//waleed
  //  String HTTP = "http://6f11f86c.ngrok.io/";
   // String HTTP = "https://mps.islamabadpolice.gov.pk/apps_stage/";
    String HTTP = "http://mps.islamabadpolice.gov.pk/apps_stage/";
    Retrofit.Builder builder = new Retrofit.Builder()
            .baseUrl(ApiInterface.HTTP)
            .addConverterFactory(GsonConverterFactory.create())
            .client(MyOkHttpClient.getHttpClient());
    ApiInterface retrofit = builder.build().create(ApiInterface.class);

//    @POST("api/users/login/")
//    Call<WebAPIResponse<User>> loginUser(@Body HashMap<String, Object> body);
//
    @POST(HTTP+"/api/police_app/report/registration")
    Call<WebAPIResponse> registeruser(@Body HashMap<String, Object> body);

    @POST(HTTP+"/api/police_app/report/verify_code")
    Call<WebAPIResponse<String>> verify_code(@Body HashMap<String, Object> body);

    @POST(HTTP+"/api/police_app/report/verify_login")
    Call<WebAPIResponse<User>> verify_login(@Body HashMap<String, Object> body);

    @POST(HTTP+"/api/police_app/report/get_policeStation")
    Call<PoliceStationData> getpolicelist();

    @POST(HTTP+"/api/police_app/report/get_important_numbers")
    Call<ImportantNumberData> getimportantnumber();

    @POST(HTTP+"/api/police_app/report/get_downloads")
    Call<DownloadData> getdownloadlist();
//
//    @GET
//    Call<Object> sendOffline(@Url String url);

    class MyOkHttpClient {

        public static OkHttpClient getHttpClient() {
            HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
            logging.setLevel(HttpLoggingInterceptor.Level.BODY);

            OkHttpClient.Builder clientBuilder = new OkHttpClient.Builder();
            clientBuilder.readTimeout(30, TimeUnit.SECONDS);
            clientBuilder.connectTimeout(20, TimeUnit.SECONDS);
            if (true) {
                clientBuilder.addInterceptor(logging);
            }
            clientBuilder.addInterceptor(new Interceptor() {
                @Override
                public Response intercept(Chain chain) throws IOException {
                    Request original = chain.request();
                    Request.Builder requestBuilder = original.newBuilder()
                            .header("Cache-Control", "no-cache")
                            .header("Content-Type", "application/json")
                            .method(original.method(), original.body());
//                    String token = "Token a1f32065fe1f5bdaf7f3075d22fccbe469a6b498";
                    Request request = requestBuilder.build();
                    return chain.proceed(request);
                }

            });
            try {
                TrustManagerFactory trustManagerFactory = null;
                try {
                    trustManagerFactory = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
                    trustManagerFactory.init((KeyStore) null);
                } catch (NoSuchAlgorithmException | KeyStoreException e) {
                    e.printStackTrace();
                }

                TrustManager[] trustManagers = new TrustManager[0];
                if (trustManagerFactory != null) {
                    trustManagers = trustManagerFactory.getTrustManagers();
                }
                if (trustManagers.length != 1 || !(trustManagers[0] instanceof X509TrustManager)) {
                    throw new IllegalStateException("Unexpected default trust managers:" + Arrays.toString(trustManagers));
                }
                X509TrustManager trustManager = (X509TrustManager) trustManagers[0];
                clientBuilder.sslSocketFactory(getSSLSocketFactory(), trustManager);
            } catch (CertificateException e) {
                e.printStackTrace();
            } catch (KeyStoreException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            } catch (KeyManagementException e) {
                e.printStackTrace();
            }


            return clientBuilder.build();
        }

        private static TrustManager[] getWrappedTrustManagers(TrustManager[] trustManagers) {
            final X509TrustManager originalTrustManager = (X509TrustManager) trustManagers[0];
            return new TrustManager[]{
                    new X509TrustManager() {
                        public X509Certificate[] getAcceptedIssuers() {
                            return originalTrustManager.getAcceptedIssuers();
                        }

                        public void checkClientTrusted(X509Certificate[] certs, String authType) {
                            try {
                                originalTrustManager.checkClientTrusted(certs, authType);
                            } catch (CertificateException ignored) {
                            }
                        }

                        public void checkServerTrusted(X509Certificate[] certs, String authType) {
                            try {
                                originalTrustManager.checkServerTrusted(certs, authType);
                            } catch (CertificateException ignored) {
                            }
                        }
                    }
            };
        }

        private static SSLSocketFactory getSSLSocketFactory()
                throws CertificateException, KeyStoreException, IOException,
                NoSuchAlgorithmException, KeyManagementException {
            CertificateFactory cf = CertificateFactory.getInstance("X.509");
            InputStream caInput;
            caInput = MyApplication.getAppContext().getResources().openRawResource(R.raw.starisb);
            Certificate ca = cf.generateCertificate(caInput);
            caInput.close();
            KeyStore keyStore = KeyStore.getInstance("BKS");
            keyStore.load(null, null);
            keyStore.setCertificateEntry("ca", ca);
            String tmfAlgorithm = TrustManagerFactory.getDefaultAlgorithm();
            TrustManagerFactory tmf = TrustManagerFactory.getInstance(tmfAlgorithm);
            tmf.init(keyStore);
            TrustManager[] wrappedTrustManagers = getWrappedTrustManagers(tmf.getTrustManagers());
            SSLContext sslContext = SSLContext.getInstance("TLS");
            sslContext.init(null, wrappedTrustManagers, null);
            return sslContext.getSocketFactory();
        }


    }
}
