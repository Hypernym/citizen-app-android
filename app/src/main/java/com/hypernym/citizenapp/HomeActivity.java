package com.hypernym.citizenapp;

import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.widget.TextView;

import com.hypernym.citizenapp.dialog.SimpleDialog;
import com.hypernym.citizenapp.fragments.HomeFragment;
import com.hypernym.citizenapp.fragments.ProfileFragment;
import com.hypernym.citizenapp.model.User;
import com.hypernym.citizenapp.toolbox.ToolbarListener;
import com.hypernym.citizenapp.utils.ActivityUtils;
import com.hypernym.citizenapp.utils.Constants;
import com.hypernym.citizenapp.utils.CustomTypefaceSpan;
import com.hypernym.citizenapp.utils.GsonUtils;
import com.hypernym.citizenapp.utils.LoginUtils;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static android.Manifest.permission.CALL_PHONE;
import static android.Manifest.permission.CAMERA;
import static android.Manifest.permission.READ_EXTERNAL_STORAGE;

/**
 * Created by Bilal Rashid on 10/10/2017.
 */

public class HomeActivity extends AppCompatActivity implements ToolbarListener, NavigationView.OnNavigationItemSelectedListener {
    private Toolbar mToolbar;
    private SimpleDialog mSimpleDialog;
    public static final int RequestPermissionCode = 1;
    DrawerLayout drawer;
    NavigationView navigationView;
    View headerLayout;
Menu menu;
    TextView textView;
    User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        toolbarSetup();
        String fragmentName = getIntent().getStringExtra(Constants.FRAGMENT_NAME);
        Bundle bundle = getIntent().getBundleExtra(Constants.DATA);
        if (!TextUtils.isEmpty(fragmentName)) {
            Fragment fragment = Fragment.instantiate(this, fragmentName);
            if (bundle != null)
                fragment.setArguments(bundle);
            addFragment(fragment);
        } else {
            addFragment(new HomeFragment());
        }
        if (Checkpermission()) {
        } else {
            requestlocationpermission();
        }
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);


        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, mToolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_navbaricon);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        View headerView = navigationView.getHeaderView(0);
        textView = (TextView) headerView.findViewById(R.id.txt_username);
        // Typeface poppinsRegular = Typeface.createFromAsset(textView.getContext().getAssets(), "Poppins-Regular.ttf");
        Typeface poppinsmedium = Typeface.createFromAsset(textView.getContext().getAssets(), "Poppins-Medium.ttf");
        textView.setTypeface(poppinsmedium);

        user = LoginUtils.getUser(getApplicationContext());
        if (user != null) {
            String upperString = user.getName().substring(0, 1).toUpperCase() + user.getName().substring(1);
            textView.setText(upperString);
        } else {
            textView.setText("Username");
        }
        setCustonFontToDrawerMenu();
//        if (!EventBus.getDefault().isRegistered(this))
//            EventBus.getDefault().register(this);
    }

    private void setCustonFontToDrawerMenu() {
        menu = navigationView.getMenu();
        for (int i=0;i<menu.size();i++) {
            MenuItem mi = menu.getItem(i);

            //for aapplying a font to subMenu ...
            SubMenu subMenu = mi.getSubMenu();
            if (subMenu!=null && subMenu.size() >0 ) {
                for (int j=0; j <subMenu.size();j++) {
                    MenuItem subMenuItem = subMenu.getItem(j);
                    applyFontToMenuItem(subMenuItem);
                }
            }

            //the method we have create in activity
            applyFontToMenuItem(mi);
        }
    }

    private void applyFontToMenuItem(MenuItem mi) {
        Typeface font = Typeface.createFromAsset(getAssets(), "Poppins-Regular.ttf");
        SpannableString mNewTitle = new SpannableString(mi.getTitle());
        mNewTitle.setSpan(new CustomTypefaceSpan("" , font), 0 , mNewTitle.length(),  Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        mi.setTitle(mNewTitle);
    }

    public void addFragment(final Fragment fragment) {
        getSupportFragmentManager().beginTransaction().replace(R.id.container, fragment).commit();
    }

    public void toolbarSetup() {

        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mToolbar.setTitle(" ");
        ActivityUtils.centerToolbarTitle(mToolbar, false);
        setSupportActionBar(mToolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(false);
        }
    }


    public boolean Checkpermission() {
        int FirstPermissionResult = ContextCompat.checkSelfPermission(getApplicationContext(), ACCESS_FINE_LOCATION);
        int SecondPermissionResult = ContextCompat.checkSelfPermission(getApplicationContext(), READ_EXTERNAL_STORAGE);
        int CallPermissonResult = ContextCompat.checkSelfPermission(getApplicationContext(), CALL_PHONE);
        int CameraPermissionResult = ContextCompat.checkSelfPermission(getApplicationContext(), CAMERA);


        return FirstPermissionResult == PackageManager.PERMISSION_GRANTED &&
                SecondPermissionResult == PackageManager.PERMISSION_GRANTED &&
                CallPermissonResult == PackageManager.PERMISSION_GRANTED &&
                CameraPermissionResult == PackageManager.PERMISSION_GRANTED;
    }

    private void requestlocationpermission() {
        ActivityCompat.requestPermissions(HomeActivity.this, new String[]
                {
                        ACCESS_FINE_LOCATION,
                        READ_EXTERNAL_STORAGE,
                        CALL_PHONE,
                        CAMERA


                }, RequestPermissionCode);


    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {

            case RequestPermissionCode:

                if (grantResults.length > 0) {

                    boolean LocationPermission = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                    boolean StoragePermission = grantResults[1] == PackageManager.PERMISSION_GRANTED;
                    boolean CallPermission = grantResults[1] == PackageManager.PERMISSION_GRANTED;
                    boolean camerpermission = grantResults[2] == PackageManager.PERMISSION_GRANTED;

                    if (LocationPermission && StoragePermission && CallPermission && camerpermission) {

                        // Toast.makeText(HomeActivity.this, "Permission Granted", Toast.LENGTH_LONG).show();
                    } else {
                        //Toast.makeText(HomeActivity.this,"Permission Denied",Toast.LENGTH_LONG).show();

                    }
                }

                break;
        }
    }

    @Override
    public void setTitle(String title) {
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(title);
        }

    }

    @Override
    public void onBackPressed() {

        mSimpleDialog = new SimpleDialog(this, null, getString(R.string.msg_exit),
                getString(R.string.button_cancel), getString(R.string.button_ok), new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switch (view.getId()) {
                    case R.id.button_positive:
                        mSimpleDialog.dismiss();
                        HomeActivity.this.finish();
                        break;
                    case R.id.button_negative:
                        mSimpleDialog.dismiss();
                        break;
                }
            }
        });
        mSimpleDialog.show();

    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.home) {

            addFragment(new HomeFragment());
            drawer.closeDrawer(Gravity.START);
            return true;

        } else if (item.getItemId() == R.id.profile) {
            addFragment(new ProfileFragment());
            drawer.closeDrawer(Gravity.START);
            return true;
        } else if (item.getItemId() == R.id.help) {
            addFragment(new HomeFragment());
            drawer.closeDrawer(Gravity.START);
            return true;
        } else if (item.getItemId() == R.id.signout) {
            ActivityUtils.startActivity(this, LoginActivity.class, true);
            LoginUtils.clearUser(getApplicationContext());
            // LoginUtils.removeAuthToken(getApplicationContext());
            drawer.closeDrawer(Gravity.START);
            return true;
        }
        return true;

    }
}
