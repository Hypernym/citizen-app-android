package com.hypernym.citizenapp.utils;

import android.content.Context;
import android.content.Intent;

import com.hypernym.citizenapp.model.User;


public class LoginUtils {

    public static boolean isUserLogin(Context context) {
        return PrefUtils.getBoolean(context, Constants.USER_AVAILABLE, false);
    }

    public static void saveUser(Context context, User user) {
        if (user == null)
            return;

        PrefUtils.persistString(context, Constants.USER, GsonUtils.toJson(user));
    }
    public static User getUser(Context context) {
        return GsonUtils.fromJson(PrefUtils.getString(context, Constants.USER), User.class);
    }

    public static void userLoggedIn(Context context) {
        PrefUtils.persistBoolean(context, Constants.USER_AVAILABLE, true);
    }

    public static void saveUserToken(Context context, String token) {
        if (token == null)
            return;

        PrefUtils.persistString(context, Constants.USER_TOKEN, token);
    }

    public static String getUserToken(Context context) {
        return PrefUtils.getString(context, Constants.USER_TOKEN);
    }

    public static void clearUser(Context context) {
        PrefUtils.remove(context, Constants.USER_AVAILABLE);
        PrefUtils.remove(context, Constants.USER_TOKEN);
        PrefUtils.remove(context, Constants.USER);
        PrefUtils.remove(context, Constants.USER_EMAIL);

    }
}
