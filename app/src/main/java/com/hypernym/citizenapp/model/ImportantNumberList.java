package com.hypernym.citizenapp.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ImportantNumberList {

    @SerializedName("Name")
    @Expose
    private String name;
    @SerializedName("Icon")
    @Expose
    private String icon;
    @SerializedName("Number")
    @Expose
    private String number;
    @SerializedName("CanCall")
    @Expose
    private Integer canCall;
    @SerializedName("CanSms")
    @Expose
    private Integer canSms;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public Integer getCanCall() {
        return canCall;
    }

    public void setCanCall(Integer canCall) {
        this.canCall = canCall;
    }

    public Integer getCanSms() {
        return canSms;
    }

    public void setCanSms(Integer canSms) {
        this.canSms = canSms;
    }

}