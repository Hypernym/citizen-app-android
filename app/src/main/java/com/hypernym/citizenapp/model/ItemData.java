package com.hypernym.citizenapp.model;

public class ItemData {


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(int imageUrl) {
        this.imageUrl = imageUrl;
    }

    private String title;
    private int imageUrl;

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    private String  number;

    public ItemData(String title,int imageUrl,String number){

        this.title = title;
        this.imageUrl = imageUrl;
        this.number = number;
    }
    // getters & setters
}