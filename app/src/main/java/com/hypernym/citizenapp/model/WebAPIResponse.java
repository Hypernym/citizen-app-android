package com.hypernym.citizenapp.model;

/**
 * Created by Metis on 22-Mar-18.
 */

public class WebAPIResponse<T> {
    public String error_msg;
    public T data;
    public String condition;
}